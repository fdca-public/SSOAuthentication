# SSOAuthentication

SSOAuthentication est une librairie pour simplifier l'authentification SSO avec OpenID Connect dans Azure.

## G�n�ration du fichier minify

npx webpack


## Importer la librairie dans le code

### Bower

```
"dependencies": {
    "SSOAuthentication": "https://gitlab.com/fdca-public/SSOAuthentication.git#1.0.0"
}

"sources": {
    "SSOAuthentication": {
        "mapping": [
            {
             "bower_components/SSOAuthentication/dist/**": "dist"
            }
        ]
    }
}
```

### index.html

`<script src="resources/lib/SSOAuthentication/dist/main.js?v=@@pom_version@@"></script>`

## Utilisation dans le code

Sur le bouton d'authentification SSO, on doit appeler la m�thode ssoLogin:

`new loginSSOController().ssoLogin("https://authenticationserverfdca.azurewebsites.net/", "http://localhost:8080/redirectedUrl");`

Voici les param�tres de la m�thode: 
- Le premier param�tre correspond au path vers le serveur d'authentification.
- Le deuxi�me param�tre � l'url � lequel on sera rediriger quand l'authentification sera faite.

Une fois le premier call r�ussi et qu'on est rediriger avec succ�ss, on doit appeler la m�thode validateCode pour finir l'authentification.

`new loginSSOController().validateCode("https://authenticationserverfdca.azurewebsites.net/", code , "http://localhost:8080/Login")`

voici les param�tres de la m�thode:
- Le premier param�tre pass� correspond au path vers le serveur d'authentification.
- le deuxi�me param�tre c'est la valeur du code dans l'url: 
![img.png](resources/images/img.png)
- le troisi�me correspond � la redirection finale une fois authentifi�.
