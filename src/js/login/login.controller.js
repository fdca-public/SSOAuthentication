import sha256 from 'crypto-js/sha256';
import Base64 from 'crypto-js/enc-base64';
const authenticationserver = require("./service/authenticationserver.service").default;

export default class {

	validateCode(authenticationServerUrl, code, redirectUri) {
		new authenticationserver().openIdLogin(authenticationServerUrl, code, this.getCookieValue("redirectUri"), this.getCookieValue("codeverifier")).then((response) => {
			document.cookie = "Authorization=" + response.headers['new-authorization'];
			document.cookie = "Refresh-Token=" + response.headers['new-refresh-token'];

			window.location.href = redirectUri;
		});
	}

	ssoLogin(authenticationServerUrl, redirectUri) {
		let base64URLEncode = this.base64URLEncode;
		let codeVerifier = this.generateCodeVerifier();
		let encodeCodeChallenge = this.encodeCodeChallenge;

		new authenticationserver().getOpenIdConfiguration(authenticationServerUrl).then((response) => {
				let openIdConfiguration = response.data;
				document.cookie = "codeverifier=" + codeVerifier;
			    document.cookie = "redirectUri=" + redirectUri;

				let codeChallenge = base64URLEncode(encodeCodeChallenge(codeVerifier));

				window.location.href = openIdConfiguration.endpointTokenUrl +
					`?client_id=${encodeURI(openIdConfiguration.clientId)}` +
					`&response_type=${encodeURI(openIdConfiguration.responseType)}` +
					`&redirect_uri=${encodeURI(redirectUri)}` +
					`&scope=${encodeURI(openIdConfiguration.scope)}` +
					`&response_mode=${encodeURI(openIdConfiguration.responseMode)}` +
					`&code_challenge=${encodeURI(base64URLEncode(codeChallenge))}` +
					`&code_challenge_method=${encodeURI("S256")}`
			}
		);
	};

	encodeCodeChallenge(codeVerifier) {
		let hash = sha256(codeVerifier)

		return Base64.stringify(hash);
	}

	generateCodeVerifier() {
		return this.base64URLEncode(btoa(this.randomString(32)));
	}

	randomString(number) {
		let result = "";
		const characters = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789";
		const charactersLength = characters.length;
		for (let i = 0; i < number; i++) {
			result += characters.charAt(Math.floor(this.getRandomNumber() * charactersLength));
		}
		return result;
	}

	base64URLEncode(value) {
		return value
			.split("+").join("-")
			.split("/").join("_")
			.split("=").join("");
	}

	getRandomNumber() {
		return crypto.getRandomValues(new Uint32Array(1))[0] / 10000000000;
	}

	getCookieValue(name) {
		var match = document.cookie.match(new RegExp('(^| )' + name + '=([^;]+)'));
		if (match) return match[2];
	}
}