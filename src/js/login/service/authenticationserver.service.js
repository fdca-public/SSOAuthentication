import axios from 'axios';

export default class {
	getOpenIdConfiguration(serverPath) {
		return axios.get(serverPath + "api/authentication/configuration/openid");
	};

	openIdLogin(serverPath, code, redirectUri, codeVerifier) {
		let openIdLogin = {
			code: code,
			codeVerifier: codeVerifier,
			redirectUri: redirectUri
		};

		return axios.post(serverPath + "api/authentication/openid-login", openIdLogin);
	};
}